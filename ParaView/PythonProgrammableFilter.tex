Any of the pipeline modules in \ParaView are doing one of two things: They are
either generating data or processing input data. The generation of data can
happen either using mathematical models (any of the sources created from the
\menu{Sources} menu) or by reading files on disks or other media. \ParaView
includes several types of readers and filters, many (if not all) of which are
written using C++ by the \ParaView and VTK developers and then compiled into
the executables and runtime libraries.

One of the primary focuses of \ParaView since its early development is
customizability. We wanted to enable developers and users to plug in their own
code to read custom file formats or to execute special algorithms for data
processing. Thus, an extensive plugin infrastructure was born, enabling
developers to write C++ code that can be compiled into plugins that can be imported
into the \ParaView executables at runtime. While such plugins are immensely
flexible, the process can be tedious and overwhelming, since developers would
need to familiarize themselves with the large C++ APIs provided by VTK and
\ParaView. Hence, easier mechanisms were needed to enable \ParaView end users to
create such custom sources and filters more easily. Thus, the programmable
modules, which includes the \ui{Programmable Source} and \ui{Programmable Filter},
were born.

With these programmable modules, you can write Python scripts that get executed
by \ParaView to generate and process data, just like the other C++ modules. Since
the programming environment is Python, it means that you have access to a plethora
of Python packages including NumPy and SciPy. %\fixme{references}.
By using such
packages, along with the data processing API provided by the \ParaView, you can
quickly put together readers, and filters to extend and customize \ParaView.

In this chapter, we will look at writing \ui{Programmable Source}s and
\ui{Programmable Filter}s using \ParaView's Python-based data processing API
through a collection of recipes and examples. A thorough discussion on the data
processing API itself is covered in Chapter~\ref{chapter:VTKNumPyIntegration}.

\begin{commonerrors}
  In this guide so far, we have been looking at examples of Python scripts for
  \pvpython. These scripts are used to script the actions you would
  perform using the \paraview UI. The scripts you would
  write for \ui{Programmable Source} and \ui{Programmable Filter} are entirely
  different. The data processing API executes within the data processing
  pipeline and, thus, has access to the data being processed. In client-server
  mode, this means that such scripts are indeed executed on the server side,
  potentially in parallel, across several MPI ranks. Therefore, attempting to import
  the \py{paraview.simple} Python module in the \ui{Programmable Source} script, for
  example, is not supported and will have unexpected consequences.
\end{commonerrors}

\section{Understanding the programmable modules}
\label{sec:UnderstandingProgrammableModules}

With programmable modules, you are writing custom code for filters and sources.
You are expected to understand the basics of a VTK (and \ParaView) data
processing pipeline, including the various stages of the pipeline execution as
well as the data model. Refer to Section~\ref{sec:VTKDataModel} for an overview
of the VTK data model. While a detailed discussion of the VTK pipeline execution
model is beyond the scope of this book, the fundamentals covered in this section,
along with the examples in the rest of this chapter, should give you enough to
get started and write useful scripts.

To create the programmable source or filter in \paraview,
you use the \menu{Sources > Programmable Source} or \menu{Filters > Programmable Filter}
menus, respectively. Since the \ui{Programmable Filter} is a filter,
like other filters, it gets connected to the currently active source(s), i.e., the
currently active source(s) become the input to this new filter. \ui{Programmable
Source}, on the other hand, does not have any inputs.

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/ProgrammableFilterInParaView.png}
\caption{\ui{Properties} panel for \ui{Programmable Filter} in \paraview.}
\label{fig:ProgrammableFilterInParaView}
\end{center}
\end{figure}

One of the first things that you specify after creating any of these programmable
modules is the \ui{Output Data Set Type}. This option lets you select the type
of dataset this module will produce. The options provided include several of the
data types discussed in Section~\ref{sec:VTKDataModel}. Additionally, for the
\ui{Programmable Filter}, you can select \ui{Same as Input} to indicate that
the filter preserves the input dataset type.

Next is the primary part: the \ui{Script}. This is where you enter the Python
script to generate or process from the inputs the dataset that the module will
produce. As with any Python script, you can import other Python packages and
modules in this script. Just be aware that when running in client-server mode,
this script is going to be executed on the server side. Accordingly, any modules or
packages you import must be available on the server side to avoid errors.

This script gets executed in what's called the \emph{RequestData} pass of the
pipeline execution. This is the pipeline pass in which an algorithm is expected
to produce the output dataset.


There are several other passes in a pipeline's execution. The ones for which you
can specify a Python script to execute with these programmable modules are:

\begin{itemize}
  \item \emph{RequestInformation}: In this pass, the algorithm is expected to
    provide the pipeline with any meta-data available about the data that will be
    produced by it. This includes things like number of timesteps in the dataset
    and their time values for temporal datasets or extents for
    structured datasets. This gets called before \emph{RequestData} pass. In the
    \emph{RequestData} pass, the pipeline could potentially qualify the request
    based on the meta-data provided in this pass. (E.g., if an algorithm announces
    that the temporal dataset has multiple timesteps, in \ui{RequestData}, the
    pipeline could request the algorithm in a specific timestep to produce.)

  \item \emph{RequestUpdateExtent}: In this pass, a filter gets the opportunity
    to qualify requests for execution passed on to the upstream pipeline. (E.g., if
    an upstream reader announced in its \emph{RequestInformation} that it can
    produce several timesteps, in \emph{RequestUpdateExtent}, this filter can
    make a request to the upstream reader for a specific timestep.) This pass
    gets called after \emph{RequestInformation}, but before \ui{RequestData}.
    It's not very common to provide a script for this pass.
\end{itemize}

You can specify the script for \emph{RequestInformation} pass in
\ui{RequestInformation Script} and for \emph{RequestUpdateExtent} pass in
\ui{RequestUpdateExtent Script}. Since \emph{RequestUpdateExtent} pass does not make
much sense for an algorithm that does not have any inputs, \ui{RequestUpdateExtent Script}
is not available on \ui{Programmable Source}.

\section{Recipes for Programmable Source}

In this section, we look at several recipes for \ui{Programmable Source}. A
common use of \ui{Programmable Source} is to prototype readers. If your reader
library already provides a Python API, then you can easily import the
appropriate Python package to read your dataset using \ui{Programmable
Source}.

\begin{didyouknow}
  Most of the examples in this chapter use a NumPy-centric API for accessing
  and creating data arrays. Additionally, you can use VTK's
  Python wrapped API for creating and accessing arrays. Given the omnipresence
  of NumPy, there is rarely any need for using this API directly, however.
\end{didyouknow}
\subsection{Reading a CSV file}
\label{sec:ReadingACSVFile}

For this example, we will read in a CSV file to produce a Table
(Section~\ref{sec:VTKDataModel:Table}) using \ui{Programmable Source}. We will
use NumPy to do the parsing of the CSV files and pass the arrays read in
directly to the pipeline.

\ui{Output DataSet Type} must be set to \ui{vtkTable}.

\begin{python}
# Code for 'Script'

# We will use NumPy to read the csv file.
# Refer to NumPy documentation for genfromtxt() for details on
# customizing the CSV file parsing.

import numpy as np
# assuming data.csv is a CSV file with the 1st row being the names names for
# the columns
data = np.genfromtxt("data.csv", dtype=None, names=True, delimiter=',', autostrip=True)
for name in data.dtype.names:
   array = data[name]

   # You can directly pass a NumPy array to the pipeline.
   # Since ParaView expects all arrays to be named, you
   # need to assign it a name in the 'append' call.
   output.RowData.append(array, name)
\end{python}


% TODO: Add a did you know or something to indicate how to read only on the root
% node.

\subsection{Reading a CSV file series}
\label{sec:ReadingACSVFileSeries}

Building on the example from Section ~\ref{sec:ReadingACSVFile},
let's say we have a series of files that we
want to read in as a temporal series. Recall from
Section~\ref{sec:UnderstandingProgrammableModules} that meta-data about data to
be produced, including timestep information, is announced in
\emph{RequestInformation} pass. Hence, for this example, we will need to specify
the \ui{RequestInformation Script} as well.

As was true earlier, \ui{Output DataSet Type} must be set to \ui{vtkTable}.
Now, to announce the timesteps, we use the following as the
\ui{RequestInformation Script}.

\begin{python}
# Code for 'RequestInformation Script'.
def setOutputTimesteps(algorithm, timesteps):
    "helper routine to set timestep information"
    executive = algorithm.GetExecutive()
    outInfo = executive.GetOutputInformation(0)

    outInfo.Remove(executive.TIME_STEPS())
    for timestep in timesteps:
        outInfo.Append(executive.TIME_STEPS(), timestep)

    outInfo.Remove(executive.TIME_RANGE())
    outInfo.Append(executive.TIME_RANGE(), timesteps[0])
    outInfo.Append(executive.TIME_RANGE(), timesteps[-1])

# As an example, let's say we have 4 files in the file series that we
# want to say are producing time 0, 10, 20, and 30.
setOutputTimesteps(self, (0, 10, 20, 30))
\end{python}

The \ui{Script} is similar to earlier, except that based on which timestep was
requested, we will read a specific CSV file.

\begin{python}
# Code for 'Script'
def GetUpdateTimestep(algorithm):
    """Returns the requested time value, or None if not present"""
    executive = algorithm.GetExecutive()
    outInfo = executive.GetOutputInformation(0)
    return outInfo.Get(executive.UPDATE_TIME_STEP()) \
              if outInfo.Has(executive.UPDATE_TIME_STEP()) else None

# This is the requested time-step. This may not be exactly equal to the
# timesteps published in RequestInformation(). Your code must handle that
# correctly
req_time = GetUpdateTimestep(self)

# Now, use req_time to determine which CSV file to read and read it as before.
# Remember req_time need not match the time values put out in
# 'RequestInformation Script'. Your code need to pick an appropriate file to
# read, irrespective.

...
# TODO: Generate the data as you want.

# Now mark the timestep produced.
output.GetInformation().Set(output.DATA_TIME_STEP(), req_time)
\end{python}

\subsection{Reading a CSV file with particles}
\label{sec:ReadingACSVFileWithPoints}

This is similar to Section~\ref{sec:ReadingACSVFile}. Now, however, let's say the CSV has three
columns named ``X'', ``Y'' and ``Z'' that we want to treat as point coordinates and
produce a \ui{vtkPolyData} with points instead of a \ui{vtkTable}. For that, we first
ensure that \ui{Output DataSet Type} is set to \ui{vtkPolyData}. Next, we use
the following \ui{Script}:

\begin{python}
# Code for 'Script'

from vtk.numpy_interface import algorithms as algs
from vtk.numpy_interface import dataset_adapter as dsa
import numpy as np

# assuming data.csv is a CSV file with the 1st row being the names names for
# the columns
data = np.genfromtxt("/tmp/points.csv", dtype=None, names=True, delimiter=',', autostrip=True)

# convert the 3 arrays into a single 3 component array for
# use as the coordinates for the points.
coordinates = algs.make_vector(data["X"], data["Y"], data["Z"])

# create a vtkPoints container to store all the
# point coordinates.
pts = vtk.vtkPoints()

# numpyTovtkDataArray is needed to called directly to convert the NumPy
# to a vtkDataArray which vtkPoints::SetData() expects.
pts.SetData(dsa.numpyTovtkDataArray(coordinates, "Points"))

# set the pts on the output.
output.SetPoints(pts)

# next, we define the cells i.e. the connectivity for this mesh.
# here, we are creating merely a point could, so we'll add
# that as a single poly vextex cell.
numPts = pts.GetNumberOfPoints()
# ptIds is the list of point ids in this cell
# (which is all the points)
ptIds = vtk.vtkIdList()
ptIds.SetNumberOfIds(numPts)
for a in xrange(numPts):
    ptIds.SetId(a, a)

# Allocate space for 1 cell.
output.Allocate(1)
output.InsertNextCell(vtk.VTK_POLY_VERTEX, ptIds)

# We can also pass all the array read from the CSV
# as point data arrays.
for name in data.dtype.names:
    array = data[name]
    output.PointData.append(array, name)
\end{python}

The thing to note is that this time, we need to define the geometry and topology for
the output dataset. Each data type has different requirements on how these are
specified. For example, for unstructured datasets like vtkUnstructuredGrid and
vtkPolyData, we need to explicitly specify the geometry and all the
connectivity information. For vtkImageData, the geometry is defined using
origin, spacing, and extents, and connectivity is implicit.

\subsection{Reading binary 2D image}

This recipe shows how to read raw binary data representing a 3D volume. Since
raw binary files don't encode information about the volume extents and data
type, we will assume that the extents and data type are known and fixed.

For producing image volumes, you need to provide the information about the
structured extents in \emph{RequestInformation}. Ensure that the \ui{Output
Data Set Type} is set to \ui{vtkImageData}.

\begin{python}
# Code for 'RequestInformation Script'.
executive = self.GetExecutive()
outInfo = executive.GetOutputInformation(0)
# we assume the dimensions are (48, 62, 42).
outInfo.Set(executive.WHOLE_EXTENT(), 0, 47, 0, 61, 0, 41)
outInfo.Set(vtk.vtkDataObject.SPACING(), 1, 1, 1)
\end{python}

The \ui{Script} to read the data can be written as follows.

\begin{python}
# Code for 'Script'
import numpy as np

# read raw binary data.
# ensure 'dtype' is set properly.
data = np.fromfile("HeadMRVolume.raw", dtype=np.uint8)

dims = [48, 62, 42]
assert data.shape[0] == dims[0]*dims[1]*dims[2],
        "dimension mismatch"

output.SetExtent(0, dims[0]-1, 0, dims[1]-1, 0, dims[2]-1)
output.PointData.append(data, "scalars")
output.PointData.SetActiveScalars("scalars")
\end{python}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\linewidth]{Images/ReadBinaryData.png}
\caption{\ui{Programmable Source} used to read \emph{HeadMRVolume.raw} file
available in the VTK data repository.}
\label{fig:ReadBinaryData}
\end{center}
\end{figure}

\subsection{Helix source}
\label{sec:HelixProgrammableSource}

Here is another polydata source example. This time, we generate the data programmatically.

\begin{python}
# Code for 'Script'

#This script generates a helix curve.
#This is intended as the script of a 'Programmable Source'
import math
import numpy as np
from vtk.numpy_interface import algorithms as algs
from vtk.numpy_interface import dataset_adapter as dsa

numPts = 80  # Points along Helix
length = 8.0 # Length of Helix
rounds = 3.0 # Number of times around

# Compute the point coordinates for the helix.
index = np.arange(0, numPts, dtype=np.int32)
scalars = index * rounds * 2 * math.pi / numPts
x = index * length / numPts;
y = np.sin(scalars)
z = np.cos(scalars)

# Create a (x,y,z) coordinates array and associate that with
# points to pass to the output dataset.
coordinates = algs.make_vector(x, y, z)
pts = vtk.vtkPoints()
pts.SetData(dsa.numpyTovtkDataArray(coordinates, 'Points'))
output.SetPoints(pts)

# Add scalars to the output point data.
output.PointData.append(index, 'Index')
output.PointData.append(scalars, 'Scalars')

# Next, we need to define the topology i.e.
# cell information. This helix will be a single
# polyline connecting all the  points in order.
ptIds = vtk.vtkIdList()
ptIds.SetNumberOfIds(numPts)
for i in xrange(numPts):
   #Add the points to the line. The first value indicates
   #the order of the point on the line. The second value
   #is a reference to a point in a vtkPoints object. Depends
   #on the order that Points were added to vtkPoints object.
   #Note that this will not be associated with actual points
   #until it is added to a vtkPolyData object which holds a
   #vtkPoints object.
   ptIds.SetId(i, i)

# Allocate the number of 'cells' that will be added. We are just
# adding one vtkPolyLine 'cell' to the vtkPolyData object.
output.Allocate(1, 1)

# Add the poly line 'cell' to the vtkPolyData object.
output.InsertNextCell(vtk.VTK_POLY_LINE, ptIds)
\end{python}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.7\linewidth]{Images/HelixProgrammableSource.png}
\caption{\ui{Programmable Source} output generated using the script in
  Section~\ref{sec:HelixProgrammableSource}. This visualization uses \ui{Tube}
  filter to make the output polyline more prominent.
}
\label{fig:HelixProgrammableSource}
\end{center}
\end{figure}

\section{Recipes for Programmable Filter}

One of the differences between the \ui{Programmable Source} and the
\ui{Programmable Filter} is that the latter expects at least 1 input. Of course,
the code in the \ui{Programmable Filter} is free to disregard the input entirely
and work exactly as \ui{Programmable Source}. \ui{Programmable Filter} is
designed to customize data transformations. (E.g., when you want to compute derived
quantities using expressions not directly possible with \ui{Python Calculator}
and \ui{Calculator} or when you want to use other Python packages or even VTK
filters not exposed in \ParaView for processing the inputs.)

In this section we look at various recipes for \ui{Programmable Filter}s.

\subsection{Adding a new point/cell data array based on an input array}

\ui{Python Calculator} provides an easy mechanism of computing derived
variables. You can also use the \ui{Programmable Filter}.
Typically, for such cases, ensure that the \ui{Output DataSet Type} is set to
\ui{Same as Input}.

\begin{python}
# Code for 'Script'

# 'inputs' is set to an array with data objects produced by inputs to
# this filter.

# Get the first input.
input0 = inputs[0]

# compute a value.
dataArray = input0.PointData["V"] / 2.0

# To access cell data, you can use input0.CellData.

# 'output' is a variable set to the output dataset.
output.PointData.append(dataArray, "V_half")
\end{python}

The thing to note about this code is that it will work as expected even when
the input dataset is a composite dataset such as a Multiblock dataset
(Section~\ref{sec:MultiblockDataset}). Refer to Chapter~\ref{chapter:VTKNumPyIntegration}
for details on how this works. There are cases, however, when you may want to
explicitly iterate over blocks in an input multiblock dataset. For that, you can
use the following snippet.

\begin{python}
input0 = inputs[0]
if input0.IsA("vtkCompositeDataSet"):
    # iterate over all non-empty blocks in the input
    # composite dataset, including multiblock and AMR datasets.
    for block in input0:
        processBlock(block)
else:
    processBlock(input0)
\end{python}

\subsection{Computing tetrahedra volume}

This recipe computes the volume for each tetrahedral cell in the input dataset.
You can always simply use the \ui{Python Calculator} to compute cell volume
using the expression \texttt{volume(inputs[0])}. This recipe is provided to
illustrate the API.

Ensure that the output type is set to \ui{Same as Input}, and this filter assumes
the input is an unstructured grid (Section~\ref{sec:VTKDataModel:UnstructuredGrid}).

\begin{python}
# Code for 'Script'.

import numpy as np

# This filter computes the volume of the tetrahedra in an unstructured mesh.
# Note, this is just an illustration and not  the most efficient way for
# computing cell volume. You should use 'Python Calculator' instead.
input0 = inputs[0]

numTets = input0.GetNumberOfCells()

volumeArray = np.empty(numTets, dtype=np.float64)
for i in xrange(numTets):
       cell = input0.GetCell(i)
       p1 = input0.GetPoint(cell.GetPointId(0))
       p2 = input0.GetPoint(cell.GetPointId(1))
       p3 = input0.GetPoint(cell.GetPointId(2))
       p4 = input0.GetPoint(cell.GetPointId(3))
       volumeArray[i] = vtk.vtkTetra.ComputeVolume(p1,p2,p3,p4)

output.CellData.append(volumeArray, "Volume")
\end{python}

\subsection{Labeling common points between two datasets}

In this example, the programmable filter to takes two input datasets: A and B. It
outputs dataset B with a new scalar array that labels the points in B that
are also in A. You should select two datasets in the pipeline browser and then
apply the programmable filter.

\begin{python}
# Code for 'Script'

# Get the two inputs
A = inputs[0]
B = inputs[1]
# use len(inputs) to determine now many inputs are connected
# to this filter.

# We use numpy.in1d to test all which point coordinate components
# in  B are present in A as well.
maskX = np.in1d(B.Points[:,0], A.Points[:,0])
maskY = np.in1d(B.Points[:,1], A.Points[:,1])
maskZ = np.in1d(B.Points[:,2], A.Points[:,2])

# Combining each component mask, we get the mask for point
# itself.
mask = maskX & maskY & maskZ

# Now convert it to uint8, since bool arrays
# cannot be passed back to the VTK pipeline.
mask = np.asarray(mask, dtype=np.uint8)

# Initialize the output and add the labels array

# This ShallowCopy is needed since by default the output is
# initialized to be a shallow copy of the first input (inputs[0]),
# but we want it to be a description of the second input.
output.ShallowCopy(B.VTKObject)
output.PointData.append(mask, "labels")
\end{python}

% The following subsection is no longer needed because you can indeed color
% points by floats and double arrays.
% \subsection{Coloring points}
%
% Let's consider a case where you have three scalar colors (``R'', ``G'', and ``B'')
% available on the point data. Let's say each of these has values in the range
% $[0,1]$, and we want to use these for coloring the points directly without using
% a color map.
%
% Remember, to not use a color map, you uncheck \ui{Map Scalars} in the \ui{Display} properties
% section on the \ui{Properties} panel. However, the array being color with needs to be
% an unsigned-char array with values in the range $[0, 255]$ for each component. So,
% we'll need to convert the three scalar arrays into a single vector and then scale it
% too. This can be done as follows:

% \begin{python}
% # Code for 'Script'

% from vtk.numpy_interface import algorithms as algs
% import numpy as np

% r = inputs[0].PointData["R"]
% g = inputs[0].PointData["G"]
% b = inputs[0].PointData["B"]

% # combine components into a single array.
% rgb = algs.make_vector(r, g, b)

% # now scale and convert the type to uint8 ==> unsigned char
% rgb = np.asarray(rgb * 255.0, dtype=np.uint8)

% # Add the array
% output.PointData.append(rgb, "RGBColors")
% \end{python}

% As before, this will work just fine for composite datasets too, without having to
% iterate over the blocks in the composite dataset explicitly.
