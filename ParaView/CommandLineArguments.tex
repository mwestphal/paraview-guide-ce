\section{Command Line Arguments and Environment Variables}
\label{sec:commandlinearguments}

The following is a list of options available when running \ParaView from the command line. When two options are listed, separated by a comma, either of them can be used to achieve the specified result. Unless otherwise specified, all command-line options are used on the client program. Following the list of command-line options is a set of environment variables \ParaView recognizes.

\subsection{General Options}

\begin{itemize}
\item \texttt{--data=<data\_file>}: Load the specified data file into \ParaView
\item \texttt{--disable-registry}, \texttt{-dr}: Launch \ParaView in its default state; do not load any preferences saved in \ParaView's registry file.
\item \texttt{--help}, \texttt{/?}: Display a list of the command-line arguments and their descriptions.
\item \texttt{--version}, \texttt{-V}: Display \ParaView's version number, and then exit.
\end{itemize}

\subsection{Client-Server Options}

\begin{itemize}
\item \texttt{--server}, \texttt{-s}: Tell the client process where to connect to the server. The default is \texttt{‑‑server =localhost}. This command-line option is used on the client.
\item \texttt{--client-host}, \texttt{-ch}: Tell the server process(es) where to connect to the client. The default is \texttt{‑‑client-host=localhost}. This command-line option is used on the server(s).
\item \texttt{--server-port}, \texttt{-sp}: Specify the port to use in establishing a connection between the client and the server. The default is \texttt{--server-port=11111}. If used, this argument must be specified on both the client and the server command lines, and the port numbers must match.
\item \texttt{--data-server-port}, \texttt{-dsp}: Specify the port to use in establishing a connection between the client and the data server. The default is \texttt{--data-server-port=11111}. If used, this argument must be specified on both the client and the data server command lines, and the port numbers must match.
\item \texttt{--render-server-port}, \texttt{-rsp}: Specify the port to use in establishing a connection between the client and the render server. The default is \texttt{--render-server-port=22221}. If used, this argument must be specified on both the client and the render server command lines, and the port numbers must match.
\item \texttt{--reverse-connection}, \texttt{-rc}: Cause the data and render servers to connect to the client. When using this option, the client, data server, and render server (if used) must be started with this command-line argument, and you should start the client before starting either server.
\item \texttt{--connect-id}: Using a connect ID is a security feature in client-server mode. To use this feature, pass this command-line argument with the same ID number to the client and server(s). If you do not pass the same ID number, the server(s) will be shut down.
\item \texttt{--machines}, \texttt{-m}: Use this command-line argument to pass in the network configuration file for the render server. See section Error: Reference source not found for a description of this file.
\item \texttt{--servers-file}: Use this option to specify the file that contains the configurations of the servers. This option replaces the default server configuration file. Any changes to the server list are saved to this file instead of the default server configuration file.
\end{itemize}

\subsection{Rendering Options}

\begin{itemize}
\item \texttt{--stereo}: Enable stereo rendering in \ParaView.
\item \texttt{--stereo-type}: Set the stereo rendering type. Options are:
        Crystal Eyes,
        Red-Blue,
        Interlaced ( Default ),
        Dresden,
        Anaglyph,
        Checkerboard, and
        SplitViewportHorizontal * since 3.98
\item \texttt{--tile-dimensions-x}, \texttt{-tdx}: Specify the number of tiles in the horizontal direction of the tiled display (\texttt{--tile-dimensions-x=<number\_of\_tiles>}). This value defaults to 0. To use this option, you must be running in client/server or client/data server/render server mode, and this option must be specified on the client command line. Setting this option to a value greater than 0 will enable the tiled display. If this argument is set to a value greater than 0 and \texttt{-tdy} (see below) is not set, then \texttt{-tdy} will default to 1.
\item \texttt{--tile-dimensions-y}, \texttt{-tdy}: Specify the number of tiles in the vertical direction of the tiled display (\texttt{--tile-dimensions-y=<number\_of\_tiles>}). This value defaults to 0. To use this option, you must be running in client/server or client/data server/render server mode, and this option must be specified on the client command line. Setting this option to a value greater than 0 will enable the tiled display. If this argument is set to a value greater than 0 and \texttt{-tdx} (see above) is not set, then \texttt{-tdx} will default to 1.
\item \texttt{--tile-mullion-x}, \texttt{-tmx}: Specify the spacing (in pixels) between columns in tiled display images.
\item \texttt{--tile-mullion-y}, \texttt{-tmy}: Specify the spacing (in pixels) between rows in tiled display images.
\item \texttt{--use-offscreen-rendering}: Use offscreen rendering on the satellite processes. On Unix platforms, software rendering or mangled Mesa must be used with this option.
\item \texttt{--disable-composite}, \texttt{-dc}: Use this command-line option if the data server does not have rendering resources and you are not using a render server. All the rendering will then be done on the client.
\end{itemize}

\subsection{Executable Help}

\texttt{paraview --help}

\begin{shell}
 --connect-id=opt      Set the ID of the server and client to make sure they match.

 --cslog=opt           ClientServerStream log file.

 --data=opt            Load the specified data. To specify file series replace the numeral with a '.' eg. my0.vtk, my1.vtk...myN.vtk becomes my..vtk

 --data-directory=opt  Set the data directory where test-case data are.

 --disable-light-kit   When present, disables light kit by default. Useful for dashboard tests.

 --disable-registry
 -dr                   Do not use registry when running ParaView (for testing).

 --exit                Exit application when testing is done. Use for testing.

 --help
 /?                    Displays available command line arguments.

 --machines=opt
 -m=opt                Specify the network configurations file for the render server.

 --multi-servers       Allow client to connect to several pvserver

 --script=opt          Set a python script to be evaluated on startup.

 --server=opt
 -s=opt                Set the name of the server resource to connect with when the client starts.

 --server-url=opt
 -url=opt              Set the server-url to connect with when the client starts. --server (-s) option supersedes this option, hence one should only use one of the two options.

 --servers-file=opt    Load the specified server configuration file (.pvsc). This option replaces the default server configuration file. Any changes to the server list are saved to this file instead of the default server configuration file.

 --state=opt           Load the specified statefile (.pvsm).

 --stereo              Tell the application to enable stereo rendering

 --stereo-type=opt     Specify the stereo type. This valid only when --stereo is specified. Possible values are "Crystal Eyes", "Red-Blue", "Interlaced", "Dresden", "Anaglyph", "Checkerboard"

 --test-baseline=opt   Add test baseline. Can be used multiple times to specify multiple baselines for multiple tests, in order.

 --test-directory=opt  Set the temporary directory where test-case output will be stored.

 --test-master         (For testing) When present, tests master configuration.

 --test-script=opt     Add test script. Can be used multiple times to specify multiple tests.

 --test-slave          (For testing) When present, tests slave configuration.

 --test-threshold=opt  Add test image threshold. Can be used multiple times to specify multiple image thresholds for multiple tests in order.

 --version
 -V                    Give the version number and exit.
\end{shell}

\texttt{pvbatch --help}

\begin{shell}
 --cslog=opt                ClientServerStream log file.

 --help
 /?                         Displays available command line arguments.

 --machines=opt
 -m=opt                     Specify the network configurations file for the render server.

 --symmetric
 -sym                       When specified, the python script is processed symmetrically on all processes.

 --use-offscreen-rendering  Render offscreen on the satellite processes. This option only works with software rendering or mangled mesa on Unix.

 --version
 -V                         Give the version number and exit.
\end{shell}

\texttt{pvpython --help}

\begin{shell}
 --connect-id=opt   Set the ID of the server and client to make sure they match.

 --cslog=opt        ClientServerStream log file.

 --data=opt         Load the specified data. To specify file series replace the numeral with a '.' eg. my0.vtk, my1.vtk...myN.vtk becomes my..vtk

 --help
 /?                 Displays available command line arguments.

 --machines=opt
 -m=opt             Specify the network configurations file for the render server.

 --multi-servers    Allow client to connect to several pvserver

 --state=opt        Load the specified statefile (.pvsm).

 --stereo           Tell the application to enable stereo rendering

 --stereo-type=opt  Specify the stereo type. This valid only when --stereo is specified. Possible values are "Crystal Eyes", "Red-Blue", "Interlaced", "Dresden", "Anaglyph", "Checkerboard"

 --version
 -V                 Give the version number and exit.
\end{shell}

\texttt{pvserver --help}

\begin{shell}
--client-host=opt
 -ch=opt                    Tell the data|render server the host name of the client, use with -rc.

 --connect-id=opt           Set the ID of the server and client to make sure they match.

 --cslog=opt                ClientServerStream log file.

 --disable-composite
 -dc                        Use this option when rendering resources are not available on the server.

 --help
 /?                         Displays available command line arguments.

 --machines=opt
 -m=opt                     Specify the network configurations file for the render server.

 --multi-clients            Allow server to keep listening for serveral client toconnect to it and share the same visualization session.

 --reverse-connection
 -rc                        Have the server connect to the client.

 --server-port=opt
 -sp=opt                    What port should the combined server use to connect to the client. (default 11111).

 --tile-dimensions-x=opt
 -tdx=opt                   Size of tile display in the number of displays in each row of the display.

 --tile-dimensions-y=opt
 -tdy=opt                   Size of tile display in the number of displays in each column of the display.

 --tile-mullion-x=opt
 -tmx=opt                   Size of the gap between columns in the tile display, in Pixels.

 --tile-mullion-y=opt
 -tmy=opt                   Size of the gap between rows in the tile display, in Pixels.

 --timeout=opt              Time (in minutes) since connecting with a client after which the server may timeout. The client typically shows warning messages before the server times out.

 --use-offscreen-rendering  Render offscreen on the satellite processes. This option only works with software rendering or mangled mesa on Unix.

 --version
 -V                         Give the version number and exit.
\end{shell}

\texttt{pvdataserver --help}

\begin{shell}
 --client-host=opt
 -ch=opt                 Tell the data|render server the host name of the client, use with -rc.

 --connect-id=opt        Set the ID of the server and client to make sure they match.

 --cslog=opt             ClientServerStream log file.

 --data-server-port=opt
 -dsp=opt                What port data server use to connect to the client. (default 11111).

 --help
 /?                      Displays available command line arguments.

 --machines=opt
 -m=opt                  Specify the network configurations file for the render server.

 --multi-clients         Allow server to keep listening for serveral client toconnect to it and share the same visualization session.

 --reverse-connection
 -rc                     Have the server connect to the client.

 --timeout=opt           Time (in minutes) since connecting with a client after which the server may timeout. The client typically shows warning messages before the server times out.

 --version
 -V                      Give the version number and exit.
\end{shell}

\texttt{pvrenderserver --help}

\begin{shell}
 --client-host=opt
 -ch=opt                    Tell the data|render server the host name of the client, use with -rc.

 --connect-id=opt           Set the ID of the server and client to make sure they match.

 --cslog=opt                ClientServerStream log file.

 --help
 /?                         Displays available command line arguments.

 --machines=opt
 -m=opt                     Specify the network configurations file for the render server.

 --render-server-port=opt
 -rsp=opt                   What port should the render server use to connect to the client. (default 22221).

 --reverse-connection
 -rc                        Have the server connect to the client.

 --tile-dimensions-x=opt
 -tdx=opt                   Size of tile display in the number of displays in each row of the display.

 --tile-dimensions-y=opt
 -tdy=opt                   Size of tile display in the number of displays in each column of the display.

 --tile-mullion-x=opt
 -tmx=opt                   Size of the gap between columns in the tile display, in Pixels.

 --tile-mullion-y=opt
 -tmy=opt                   Size of the gap between rows in the tile display, in Pixels.

 --use-offscreen-rendering  Render offscreen on the satellite processes. This option only works with software rendering or mangled mesa on Unix.

 --version
 -V                         Give the version number and exit.
\end{shell}

\subsection{Environment Variables}

In addition to the command-line options previously listed, \ParaView also recognizes the following environment variables:

\begin{itemize}
\item \texttt{PV\_DISABLE\_COMPOSITE\_INTERRUPTS} If this variable is set to 1, it is not possible to interrupt the compositing of images in parallel rendering. Otherwise it is interruptible through mouse interaction.
\item \texttt{PV\_ICET\_WINDOW\_BORDERS} Setting this variable to 1 when running \ParaView in tiled display mode using IceT causes the render window for each tile to be the same size as the display area in \ParaView's main application window. (Normally each render window fills the whole screen when tiled display mode is used.) This feature is sometimes useful when debugging \ParaView.
\item \texttt{PV\_PLUGIN\_PATH} If you have shared libraries containing plugins you wish to load in \ParaView at startup, set this environment variable to the path for these libraries.
\item \texttt{PV\_SOFTWARE\_RENDERING} This environment variable has the same effect as setting both the \texttt{--use-software-rendering} and \texttt{--use-satellite-software} environment variables.
\item \texttt{VTK\_CLIENT\_SERVER\_LOG} If set to 1, a log file will be created for the \ParaView client, server, and render server. The log files will contain any client-server streams sent to the corresponding client, server, or render server.
\item \texttt{PV\_DEBUG\_PANELS} If set to 1 a debug message will be printed with an explaination as to why each property widget in the properties panel was created.
\end{itemize}
