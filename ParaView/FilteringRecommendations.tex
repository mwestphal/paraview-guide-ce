\subsection{Avoiding data explosion}

The pipeline model that \ParaView presents is very convenient for exploratory
visualization. The loose coupling between components provides a very flexible
framework for building unique visualizations, and the pipeline structure allows
you to tweak parameters quickly and easily.

The downside of this coupling is that it can have a larger memory footprint.
Each stage of this pipeline maintains its own copy of the data. Whenever
possible, \ParaView performs shallow copies of the data so that different stages
of the pipeline point to the same block of data in memory. However, any filter
that creates new data or changes the values or topology of the data must
allocate new memory for the result. If \ParaView is filtering a very large mesh,
inappropriate use of filters can quickly deplete all available memory.
Therefore, when visualizing large datasets, it is important to understand the
memory requirements of filters.

Please keep in mind that the following advice is intended only for when dealing
with very large amounts of data and the remaining available memory is low. When
you are not in danger of running out of memory, the following advice is not
relevant.

When dealing with structured data, it is absolutely important to know what
filters will change the data to unstructured. Unstructured data has a much
higher memory footprint, per cell, than structured data because the topology
must be explicitly written out. There are many filters in \ParaView that will
change the topology in some way, and these filters will write out the data as an
unstructured grid, because that is the only dataset that will handle any type of
topology that is generated. The following list of filters will write out a new
unstructured topology in its output that is roughly equivalent to the input.
These filters should never be used with structured data and should be used with
caution on unstructured data.

\begin{multicols}{3}
\begin{compactitem}
\item \emph{Append Datasets}
\item \emph{Append Geometry}
\item \emph{Clean}
\item \emph{Clean to Grid}
\item \emph{Connectivity}
\item \emph{D3}
\item \emph{Delaunay 2D/3D}
\item \emph{Extract Edges}
\item \emph{Linear Extrusion}
\item \emph{Loop Subdivision}
\item \emph{Reflect}
\item \emph{Rotational Extrusion}
\item \emph{Shrink}
\item \emph{Smooth}
\item \emph{Subdivide}
\item \emph{Tessellate}
\item \emph{Tetrahedralize}
\item \emph{Triangle Strips}
\item \emph{Triangulate}
\end{compactitem}
\end{multicols}

Technically, the \ui{Ribbon} and \ui{Tube} filters should fall into this list.
However, as they only work on 1D cells in poly data, the input data is usually
small and of little concern.

This similar set of filters also outputs unstructured grids, but also tends to
reduce some of this data. Be aware though that this data reduction is often
smaller than the overhead of converting to unstructured data. Also note that the
reduction is often not well balanced. It is possible (often likely) that a
single process may not lose any cells. Thus, these filters should be used with
caution on unstructured data and extreme caution on structured data.

\begin{multicols}{2}
\begin{compactitem}
\item \emph{Clip}
\item \emph{Decimate}
\item \emph{Extract Cells by Region}
\item \emph{Extract Selection}
\item \emph{Quadric Clustering}
\item \emph{Threshold}
\end{compactitem}
\end{multicols}

Similar to the items in the preceding list, \ui{Extract Subset} performs data
reduction on a structured dataset, but also outputs a structured dataset. So the
warning about creating new data still applies, but you do not have to worry
about converting to an unstructured grid.

This next set of filters also outputs unstructured data, but it also performs a
reduction on the dimension of the data (for example 3D to 2D), which results in
a much smaller output. Thus, these filters are usually safe to use with
unstructured data and require only mild caution with structured data.


\begin{multicols}{2}
\begin{compactitem}
\item \emph{Cell Centers}
\item \emph{Contour}
\item \emph{Extract CTH Fragments}
\item \emph{Extract CTH Parts}
\item \emph{Extract Surface}
\item \emph{Feature Edges}
\item \emph{Mask Points}
\item \emph{Outline (curvilinear)}
\item \emph{Slice}
\item \emph{Stream Tracer}
\end{compactitem}
\end{multicols}

The filters below do not change the connectivity of the data at all. Instead,
they only add field arrays to the data. All the existing data is shallow copied.
These filters are usually safe to use on all data.

\begin{multicols}{2}
\begin{compactitem}
\item \emph{Block Scalars}
\item \emph{Calculator}
\item \emph{Cell Data to Point Data}
\item \emph{Curvature}
\item \emph{Elevation}
\item \emph{Generate Surface Normals}
\item \emph{Gradient}
\item \emph{Level Scalars}
\item \emph{Median}
\item \emph{Mesh Quality}
\item \emph{Octree Depth Limit}
\item \emph{Octree Depth Scalars}
\item \emph{Point Data to Cell Data}
\item \emph{Process Id Scalars}
\item \emph{Random Vectors}
\item \emph{Resample with dataset}
\item \emph{Surface Flow}
\item \emph{Surface Vectors}
\item \emph{Texture Map to...}
\item \emph{Transform}
\item \emph{Warp (scalar)}
\item \emph{Warp (vector)}
\end{compactitem}
\end{multicols}

This final set of filters either add no data to the output (all data of
consequence is shallow copied) or the data they add is generally independent of
the size of the input. These are almost always safe to add under any
circumstances (although they may take a lot of time).

\begin{multicols}{2}
\begin{compactitem}
\item \emph{Annotate Time}
\item \emph{Append Attributes}
\item \emph{Extract Block}
\item \emph{Extract Datasets}
\item \emph{Extract Level}
\item \emph{Glyph}
\item \emph{Group Datasets}
\item \emph{Histogram}
\item \emph{Integrate Variables}
\item \emph{Normal Glyphs}
\item \emph{Outline}
\item \emph{Outline Corners}
\item \emph{Plot Global Variables Over Time}
\item \emph{Plot Over Line}
\item \emph{Plot Selection Over Time}
\item \emph{Probe Location}
\item \emph{Temporal Shift Scale}
\item \emph{Temporal Snap-to-Time-Steps}
\item \emph{Temporal Statistics}
\end{compactitem}
\end{multicols}

There are a few special case filters that do not fit well into any of the
previous classes. Some of the filters, currently \ui{Temporal Interpolator} and
\ui{Particle Tracer}, perform calculations based on how data changes over time.
Thus, these filters may need to load data for two or more instances of time,
which can double or more the amount of data needed in memory. The \ui{Temporal
Cache} filter will also hold data for multiple instances of time. Keep in mind
that some of the temporal filters such as the Temporal Statistics and the
filters that plot over time may need to iteratively load all data from disk.
Thus, it may take an impractically long amount of time even if does not require
any extra memory.

The \ui{Programmable Filter} is also a special case that is impossible to
classify. Since this filter does whatever it is programmed to do, it can fall
into any one of these categories.

\subsection{Culling data}

When dealing with large data, it is best to cull out data whenever possible and
do so as early as possible. Most large data starts as 3D geometry and the
desired geometry is often a surface. As surfaces usually have a much smaller
memory footprint than the volumes that they are derived from, it is best to
convert to a surface early on. Once you do that, you can apply other filters in
relative safety.

A very common visualization operation is to extract isosurfaces from a volume
using the Contour filter. The \ui{Contour} filter usually outputs geometry much
smaller than its input. Thus, the \ui{Contour} filter should be applied early if
it is to be used at all. Be careful when setting up the parameters to the
\ui{Contour} filter because it still is possible for it to generate a lot of
data which can happen if you specify many isosurface values. High frequencies
such as noise around an isosurface value can also cause a large, irregular
surface to form.

Another way to peer inside of a volume is to perform a \ui{Slice} on it. The
\ui{Slice} filter will intersect a volume with a plane and allow you to see the
data in the volume where the plane intersects. If you know the relative location
of an interesting feature in your large dataset, slicing is a good way to view
it.

If you have little \emph{a priori} knowledge of your data and would like to
explore the data without the long memory and processing time for the full
dataset, you can use the \ui{Extract Subset} filter to subsample the data. The
subsampled data can be dramatically smaller than the original data and should
still be well load balanced. Of course, be aware that you may miss small
features if the subsampling steps over them and that once you find a feature you
should go back and visualize it with the full dataset.

There are also several features that can pull out a subset of a volume:
\ui{Clip}, \ui{Threshold}, \ui{Extract Selection}, and \ui{Extract Subset} can
all extract cells based on some criterion. Be aware, however, that the extracted
cells are almost never well balanced; expect some processes to have no cells
removed. All of these filters, with the exception of \ui{Extract Subset}, will
convert structured data types to unstructured grids. Therefore, they should not
be used unless the extracted cells are of at least an order of magnitude less
than the source data.

When possible, replace the use of a filter that extracts 3D data with one that
will extract 2D surfaces. For example, if you are interested in a plane through
the data, use the \ui{Slice} filter rather than the \ui{Clip} filter. If you are
interested in knowing the location of a region of cells containing a particular
range of values, consider using the \ui{Contour} filter to generate surfaces at
the ends of the range rather than extract all of the cells with the
\ui{Threshold} filter. Be aware that substituting filters can have an effect on
downstream filters. For example, running the \ui{Histogram} filter after
\ui{Threshold} will have an entirely different effect then running it after the
roughly equivalent \ui{Contour} filter.
