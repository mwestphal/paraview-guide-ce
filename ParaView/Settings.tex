As with any large application, \paraview provides mechanisms to customize some
of its application behavior. These are referred to as \keyterm{application
settings}. or just {settings}. Such settings can be changed using the \ui{Settings} dialog,
which is accessed from  the \menu{Edit > Settings} (\menu{ParaView >
Preferences} on the Mac) menu. We have seen parts of this dialog earlier, e.g., in
Sections~\ref{sec:PropertiesPanelLayoutSettings},
\ref{sec:BasicRenderingSettings}, and \ref{sec:ParallelRenderParameters}. In
this chapter, we will take a closer look at some of the other options available
in this dialog.

The \ui{Settings} dialog is split into several tabs. The \ui{General} tab
consolidates most of the miscellaneous settings. The \ui{Camera} tab enables you
to change the mouse interaction mappings for the \ui{Render View} and similar
views. The \ui{Render View} tab, which we saw earlier in
Sections~\ref{sec:BasicRenderingSettings}
and~\ref{sec:ParallelRenderParameters}, provides options in regards to rendering in
\ui{Render View} and similar views. The \ui{Color Palette} tab is used to change
the active color palette.

Using this dialog is not much different than the \ui{Properties} panel. You have
the \ui{Search} box at the top, which allows you to search properties matching
the input text (Section~\ref{sec:PropertiesPanel:SearchBox}). The
\icon{Images/pqAdvanced26.png} button can be used to toggle between default and
advanced modes.

To apply the changes made to any of the settings, use the \ui{Apply} or \ui{OK}
buttons. \ui{OK} will apply the changes and close the dialog, while \ui{Cancel}
will reject any changes made and close the dialog. Any changes made to
the options in this dialog are persistent across sessions. (I.e., the next time you
launch \paraview, you'll still be using the same settings chosen earlier.) To
revert to the default, use the \ui{Restore Defaults} button. You can also manually
edit the setting file as in Section~\ref{sec:ConfiguringDefaultSettingsJSON}.
Furthermore, site maintainers can provide site-wide defaults for these, as is
explained in Section~\ref{sec:ConfiguringDefaultSettingsSiteWide}.

Next, we will see some of the important options available. Those that are only
available in the advanced mode are indicated as such using the
\icon{Images/pqAdvanced26.png} icon. You will either need to toggle on the
advanced options with the \icon{Images/pqAdvanced26.png} button or search for the
option using the \ui{Search} box.

\section{\texttt{General} settings}
\keyword{General Settings}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/SettingsDialog.png}
\label{fig:SettingsDialog}
\caption{\ui{Settings} dialog in \paraview showing the \ui{General} settings tab.}
\end{center}
\end{figure}

\begin{description}
\item[\ui{General Options}]~
  \begin{itemize}
  \item \ui{Show Splash Screen}: Uncheck this to not show the splash screen at
  application startup. You will need to restart \paraview to see the effect.
  \icon{Images/pqAdvanced26.png}
  \item \ui{Crash Recovery}: When this is checked, \paraview will intermittently
  save a backup state file as you make changes in the visualization pipeline.
  If \paraview crashes for some reason, then when you relaunch \paraview, it
  will provide you with a choice to load the backup state saved before the crash
  occurred. This is not $100\%$ reliable, but some users may find it useful to
  avoid loosing their visualization state due to a crash.
  \icon{Images/pqAdvanced26.png}
  \end{itemize}
  \item \ui{Show Save State On Exit}: When this is checked \paraview will prompt you
  to save a state file when you exit the application.

\item[\ui{Default View}]~
  \begin{itemize}
  \item \ui{Default View Type}: When \paraview starts up, it
  creates \ui{Render View} by default. You can use this option to change the
  type of the view that is created by default, instead of the \ui{Render View}.
  You can even pick \texttt{None} if you don't want to create any view by
  default.
  \icon{Images/pqAdvanced26.png}
  \end{itemize}

\item[\ui{Properties Panel Options}]~
  \begin{itemize}
  \item \ui{Auto Apply}: When checked, the \ui{Properties} panel will
  automatically apply any changes you make to the properties without requiring you
  to click the \ui{Apply} button. The same setting can also be toggled using the
  \icon{Images/AutoApplyIcon.png} button in the \ui{Main Controls} toolbar.
  \item \ui{Auto Apply Active Only}: This limits the auto-applying to the properties
  on the active source alone.
  \icon{Images/pqAdvanced26.png}
  \item \ui{Properties Panel Mode}: This allows you to split the \ui{Properties}
  panel into separate panels as described in
  Section~\ref{sec:PropertiesPanelLayoutSettings}.
  \icon{Images/pqAdvanced26.png}
  \end{itemize}

\item[\ui{Data Processing Options}]~
  \begin{itemize}
  \item \ui{Auto Convert Properties}: Several filters only work on one type of
  array, e.g., point data arrays or cell data arrays. Before using such filters,
  you are expected to apply the \ui{Point Data To Cell Data} or \ui{Cell Data To
  Point Data} filters. To avoid having to add these filters explicitly, you can
  check this checkbox. When checked, \ParaView will automatically convert data
  arrays as needed by filters, including converting cell array to point arrays
  and vice-versa, as well as extracting a single component from a
  multi-component array.
  \end{itemize}

\item[\ui{Color/Opacity Map Range Options}]~
  \begin{itemize}
  \item \ui{Transfer Function Reset Mode}: This setting controls when \ParaView
  will reset the ranges for color and opacity maps (or transfer functions). This
  only affects those color/opacity maps that have the \ui{Automatically rescale
  transfer functions to fit data} setting enabled (Section~\ref{sec:ColorMapping:MappingData}).
  When \ui{Grow and update on 'Apply'} is selected, \ParaView will grow the
  color/opacity map range to include the current data range every time you hit
  \ui{Apply} on the \ui{Properties} panel.
  Thus, when the data range changes, if the timestep is changed, the
  color/opacity map range won't be affected. To grow the range on change in
  timestep as well, use the \ui{Grow and update every timestep} option. Now the
  range will be updated on \ui{Apply} as well as when the timestep changes.
  \emph{Grow} indicates that the color/opacity map range will only be increased, never shrunk,
  to include the current data range. If you want the range to
  match the current data range exactly, then you should use the \ui{Clamp and
  update every timestep} option. Now the range will be clamped to the exact data
  range each time you hit \ui{Apply} on the \ui{Properties} panel or when the
  timestep changes.

  \item \ui{Scalar Bar Mode}: This settings controls how \paraview
  manages showing the color legend (or scalar bar) in \ui{Render View} and
  similar views.
  \end{itemize}

\item[\ui{Default Time Step}]~

  Whenever a dataset with timesteps is opened, this setting controls how
  \paraview will update the current time shown by the application. You can choose
  between \texttt{Leave current time unchanged, if possible}, \texttt{Go to
  first timestep}, and \texttt{Go to last timestep}.

\item[\ui{Animation}]~
  \begin{itemize}
  \item \ui{Cache Geometry For Animation}: This enables caching of geometry when
  playing animations to attempt to speed up animation playback in a loop. When
  caching is enabled, data ranges reported by the \ui{Information} panel and
  others can be incorrect, since the pipeline may not have updated.
  \icon{Images/pqAdvanced26.png}
  \item \ui{Animation Geometry Cache Limit}: When animation caching is enabled,
  this setting controls how much geometry (in kilobytes) can be cached by any
  rank. As soon as a rank's cache size reaches this limit, \ParaView will no
  longer cache the remaining timesteps.
  \icon{Images/pqAdvanced26.png}
  \end{itemize}

\item[\ui{Maximum Number of Data Representation Labels}]~

  When a selection is labeled by data attributes this is the maximum number of labels
  to use.  When the number of points/cells to label is above this value then a subset
  of this many will be labeled instaed. Too many overlapping labels becomes illegible,
  so this is set to 100 by default.

%\item[\ui{Miscellaneous}]~
%  \begin{itemize}
%  \item \ui{Inherit Representation Properties}: Set whether representations try
%  to maintain properties from an input representation, if present. For example,
%  if you \ui{Transform} the representation for a source, then any filter that
%  you connect to it and show in the same view will also get the same
%  transformation.
%  \icon{Images/pqAdvanced26.png}
%  \end{itemize}
\end{description}

\section{\texttt{Camera} settings}
\keyword{Camera Settings}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/SettingsDialogCamera.png}
\label{fig:SettingsDialogCamera}
\caption{\ui{Settings} dialog in \paraview showing the \ui{Camera} settings tab.}
\end{center}
\end{figure}

This tab allows you to control how you can interact in \ui{Render View} and
similar views. Basically, you are setting up a mapping between each of the mouse
buttons and keyboard modifiers, and the available interaction types including
\texttt{Rotate}, \texttt{Pan}, \texttt{Zoom}, etc. The dialog allows you to set
the interaction mapping separately for 3D and 2D interaction modes
(see Section~\ref{sec:RenderView:Interactions}).

\section{\texttt{Render View} settings}

Refer to Sections \ref{sec:BasicRenderingSettings} and
\ref{sec:ParallelRenderParameters} for various options available on the
\ui{Render View} tab.

\section{\texttt{Color Palette}}

\begin{figure}[htb]
\begin{center}
\includegraphics[width=0.6\linewidth]{Images/SettingsDialogColorPalette.png}
\label{fig:SettingsDialogColorPalette}
\caption{\ui{Settings} dialog in \paraview showing the \ui{Color Palette} settings tab.}
\end{center}
\end{figure}

The \ui{Color Palette} tab (Figure~\ref{fig:SettingsDialogColorPalette})
allows you to change the colors in the active color
palette. The tab lists the available color categories viz. \ui{Surface},
\ui{Foreground}, \ui{Edges}, \ui{Background}, \ui{Text}, and \ui{Selection}. You
can manually set colors to use for each of these categories or load one of the
predefined palettes using the \ui{Load Palette} option. To understand
\keyterm{color palettes}, let's look at an example.

Let's start \paraview and split the active view to create two \ui{Render View}
instances side by side. You may want to start \paraview with the \texttt{-dr}
command line argument to stop any of your current settings from interfering
with this demo. Next, show \ui{Sphere} as \ui{Wireframe} in the view
on the left, and show \ui{Cone} as \ui{Surface} in the view on the right.
Also, turn on \ui{Cube Axis} for \ui{Cone}. You will see something like
Figure~\ref{fig:ColorPaletteDemo} (left).

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.45\linewidth]{Images/ColorPaletteDemo1.png}
\includegraphics[width=0.45\linewidth]{Images/ColorPaletteDemo2.png}
\label{fig:ColorPaletteDemo}
\caption{The effect of loading the \ui{Print} color palette as
the active palette. The left is the original visualization and the right shows the
result after loading the \ui{Print} palette.}
\end{center}
\end{figure}

Now let's say you want to generate an image for printing. Typically, for
printing, you'd want the background color to be white, while the wireframes and
annotations to be colored black. To do that, one way is to go change each of the
colors for each each of the views, displays and cube-axes. You can imagine how
tedious that will get especially with larger pipelines. Alternatively, using the
\ui{Settings} dialog, change the active color palette to \ui{Print} as shown in
Figure~\ref{fig:SettingsDialogColorPalette} and then hit \ui{OK} or \ui{Apply}.
The visualization will immediately change to something like
Figure~\ref{fig:ColorPaletteDemo} (right).

Essentially, \ParaView allows you to \emph{link} any color property to one of
the color categories. When the color palette is changed, any color property
linked to a palette category will also be automatically updated to match the category color.
Figure~\ref{fig:LinkToColorPalette} shows how to link a color
property to a color palette category in the \ui{Properties} panel. Use the tiny
drop-down menu marker to make the menu pop up that shows the color palette categories.
Select any one of them to link that property with the category. The link is
automatically severed if you manually change the color by simply clicking on the
button.

\begin{figure}[ht]
\begin{center}
\includegraphics[width=0.4\linewidth]{Images/LinkToColorPalette.png}
\label{fig:LinkToColorPalette}
\caption{Popup menu allows you to link a color property to a color palette
category in the \ui{Properties} panel.}
\end{center}
\end{figure}
