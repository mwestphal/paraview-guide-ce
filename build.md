Build ParaViewGuide
===================

The ParaView Guide Community Edition is a **LaTeX** document that uses **CMake** to generate a final .pdf file. The Catalyst User's Guide can be built at the same time, in the same way.

## Prerequisites
You need CMake, Make, LaTeX, and ImageMagick.

### CMake
* See the [ParaView docs][] for obtaining CMake.

### Windows
* To get LaTex, install [MiKTeX][]
    - First, run the MiKTeX Update (Admin). You may have to run twice if it updates the updater binary.
    - MiKTeX Package Manager (Admin), search and install packages:
    > adjustbox, bclogo, catoptions, cite, collectbox, colortbl, enumitem, etoolbox, fancyhdr, floatflt, fncychap, frankenstein, ifoddpage, l3kernel, l3packages, listings, mdframed, mptopdf, ms, paralist, pgf, setspace, subfigure, symbol, url, wrapfig, xcolor, xstring
        + Unfortunately MiKTeX automatic install during build doesn't seem to work. If you discover a missing package during build, please add it to this list.
        + [mpm][] command line looks like a good option to do this all at once.
* Install [ImageMagick][], use the recommended download (Q16 precision is fine)
    - In the installer, choose 'Install legacy utilities (convert)'
* Make. I suggest using 'Git Bash' from [Git for Windows][], and adding the [MinGW][] binaries, which include `make`, to your path.

### Linux (Ubuntu)
* To get LaTeX, install a TeXLive package. I tried this, on Ubuntu:
`sudo apt install texlive-latex-base`
** This doesn't include the extra packages, same as those missing on Windows. ToDo: how to install missing packages.
* ImageMagick seems to be installed by default on Ubuntu.

## Run Cmake
Follow the instructions in [CONTRIBUTING.md][] to clone the repository.
If you clone like this:

    $ mkdir ParaViewGuide; cd ParaViewGuide
    $ git clone https://gitlab.kitware.com/paraview/paraview-guide-ce.git src

then:

    $ mkdir build; cd build
    $ cd build
    $ cmake-gui ../src -or- ccmake ../src

* click 'configure'
* You may change whether to build the ParaView Guide and/or the Catalyst User's Guide by changing the options **ENABLE_PARAVIEW_GUIDE** (default: ON) and/or **ENABLE_CATALYST_USERS_GUIDE** (default: OFF)
* Generator: I chose the 'MSYS makefiles' generator, after I added MinGW binaries to the path in my Git Bash shell.
* Click 'Advanced'. MIKTEX_BINARY_PATH should be set to something like "C:/Program Files (x86)/MiKTeX 2.9/miktex/bin/" to allow all the LaTeX binaries to be found.
* You may have to manually set the IMAGEMAGICK_CONVERT path, to something like "C:/Program Files/ImageMagick-7.0.3-Q16/convert.exe", if it complains about finding Windows convert.exe instead.

## Build

    $ make

* If any packages are missing, MiKTeX will offer to fetch them. However, this failed for me. Copy the package name from the missing .sty dialog, and then in the main MiKTeX Package Manager, search for the package name and install it. Build again.
* You will have to build at least twice, so that references can be updated correctly. You will see many warnings during the build process.
* On a successful build, the **ParaView/ParaViewUsersGuide.pdf** and/or **ParaViewCatalyst/ParaViewCatalystUsersGuide.pdf** will be generated based on the options you chose in cmake.

Note that `make` can take a long time, especially *rendering* the images.
To speed things, you can edit [ParaViewUsersGuide.tex][] file to change
the documentclass mode to `[draft]`.

    \documentclass[draft]{InsightSoftwareGuide}
This will generate the the draft PDF without any images.

[CONTRIBUTING.md]: CONTRIBUTING.md
[Git for Windows]: https://git-scm.com/downloads
[ImageMagick]: http://www.imagemagick.org/script/binary-releases.php
[MikTeX]: http://miktex.org/howto/install-miktex
[MinGW]: http://www.mingw.org/wiki/Getting_Started
[mpm]: http://docs.miktex.org/manual/mpm.html
[ParaView docs]: https://gitlab.kitware.com/paraview/paraview/blob/master/CONTRIBUTING.md
[ParaViewUsersGuide.tex]: ParaView/ParaViewUsersGuide.tex
